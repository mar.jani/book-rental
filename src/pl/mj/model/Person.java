package pl.mj.model;

import java.util.List;

public class Person {
    private String firstName;
    private String lastName;
    private String eMail;
    private String password;
    private List<BorrowedBook> currentBorrowedBooks;
    private List<BorrowedBook> booksRead;
    private Boolean activeAccount;

    public Person() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<BorrowedBook> getCurrentBorrowedBooks() {
        return currentBorrowedBooks;
    }

    public void setCurrentBorrowedBooks(List<BorrowedBook> currentBorrowedBooks) {
        this.currentBorrowedBooks = currentBorrowedBooks;
    }

    public List<BorrowedBook> getBooksRead() {
        return booksRead;
    }

    public void setBooksRead(List<BorrowedBook> booksRead) {
        this.booksRead = booksRead;
    }

    public Boolean getActiveAccount() {
        return activeAccount;
    }

    public void setActiveAccount(Boolean activeAccount) {
        this.activeAccount = activeAccount;
    }
}
