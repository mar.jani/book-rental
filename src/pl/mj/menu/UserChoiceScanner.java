package pl.mj.menu;

import java.util.Scanner;

public class UserChoiceScanner {
    public String getUserChoice (){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
