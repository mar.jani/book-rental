package pl.mj.db;

import pl.mj.model.Book;

import java.util.List;

public class BookList {
    private List<Book> books;

    public BookList() {
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
