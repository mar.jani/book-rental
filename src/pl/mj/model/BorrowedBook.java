package pl.mj.model;

import java.time.LocalDate;

public class BorrowedBook {
    private Book book;
    private LocalDate borrowDate;

    public BorrowedBook() {
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public LocalDate getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(LocalDate borrowDate) {
        this.borrowDate = borrowDate;
    }
}
